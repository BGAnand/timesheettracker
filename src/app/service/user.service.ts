import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  createUserUrl = environment.apiURL + "/user/createUser";
  loginUrl = environment.apiURL + "/user/userLogin";
  changePasswordUrl = environment.apiURL + "/user/changePassword";
  userInfoUrl = environment.apiURL + "/user/getUserById"
  updateUserUrl = environment.apiURL + "/user/updateUser"
  getAllUsersUrl=environment.apiURL + "/user/getAllUser"
  deactivateUserUrl=environment.apiURL + "/user/deactivateById"
  activateUserUrl= environment.apiURL + "/user/activateById"

  resetPasswordUrl=environment.apiURL + "/user/resetpassword"

  requestResetUrl=environment.apiURL + "/user/forgotpassword"
  //resetPasswordUrl=environment.apiURL + "/user/forgotpassword"

  constructor(private http: HttpClient) { }

  createUser(body): Observable<any> {
    // userName, email, password required
    // let details = new FormData();
    // details.append('userName',body.userName);
    // details.append('email',body.email);
    // details.append('password',body.password);
    console.log(body);
    return this.http.post<any>(this.createUserUrl, body);
  }

  userLogin(body): Observable<any> {
    //email, password
    return this.http.post<any>(this.loginUrl, body)
  }

  changePassword(body): Observable<any> {
    // currentPassword, newPassword required
    return this.http.put<any>(this.changePasswordUrl, body);
  }

  getUserDetails(): Observable<any> {
    return this.http.get(`${this.userInfoUrl}/${localStorage.getItem('id')}`);
  }

  updateUser(body): Observable<any> {
    //email and userName required
    return this.http.put(`${this.updateUserUrl}/${localStorage.getItem('id')}`,body);
  }
  getAllUsersDetails(): Observable<any>{
    return this.http.get(this.getAllUsersUrl);
  }

  deactivateUser(body): Observable<any>{
    return this.http.put(`${this.deactivateUserUrl}/${localStorage.getItem('id')}`,body);
  }
  activateUser(body): Observable<any>{
    return this.http.put(`${this.activateUserUrl}/${localStorage.getItem('id')}`,body);
  }

  requestReset(body): Observable<any>{
    //console.log(this.requestResetUrl);
    //console.log(body);
    return this.http.post(this.requestResetUrl, body)
  }

  // verifyUser(data): Observable < any > {
  //   return this.http.post(environment.serverUrl + 'verifyUser', data).pipe(
  //     catchError((error: HttpErrorResponse) => throwError(error))
  //   )
  // }
  
   responseReset(body, resetToken){
     let url = this.resetPasswordUrl + "/" + resetToken;
     return this.http.put(url,body);
   }
}