import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AuthGuardGuard } from 'src/app/util/authGuard/auth-guard.guard';



const routes: Routes = [
  {
    path: '', component: DashboardComponent,
    children: [
      {
        path: '', redirectTo: 'profile', pathMatch: 'full'
      },
      {
        path: 'profile',
        loadChildren: './profile/profile.module#ProfileModule',
        // canActivate: [AuthGuardGuard]
      },
      {
        path: 'timesheet',
        loadChildren: './timesheet/timesheet.module#TimesheetModule',
        // canActivate: [AuthGuardGuard],
      },
      {
        path: 'charts',
        loadChildren: './charts/charts.module#ChartsModule',
        // canActivate: [AuthGuardGuard]
      },
      {
        path: 'user',
        loadChildren: './user/user.module#UserModule',
        // canActivate: [AuthGuardGuard]
      }
    ]
  }
];

// redirectTo: 'dashboard', pathMatch: 'full'

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
