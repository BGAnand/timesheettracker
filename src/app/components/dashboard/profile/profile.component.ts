import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  userInfo: FormGroup;
  userDetails = {};

  constructor(private userService: UserService,private formBuilder:FormBuilder,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.initForm();
    this.getUserDetails();
  }

  initForm(): void {
    this.userInfo = this.formBuilder.group({
      userName: [''],
      email: [''],
      // id: ['']
    })
  }

  getUserDetails(): void {
    this.userService.getUserDetails().subscribe(res => {
      if (res.result) {
        this.userDetails = res.result;
        this.userInfo.patchValue(res.result);
        console.log(this.userInfo);
        // this.userInfo = res.result;
      }
    })
  }

  updateUserInfo(): void {
    this.userService.updateUser(this.userInfo.value).subscribe(res => {
      this.getUserDetails();
      // this.userDetails = res.result;
      // this.userInfo.patchValue(res.result);
      this.snackBar.open('User info updated','close',{
        duration: 2000
      })
    },err => {
      this.snackBar.open('Unable to update','please try again!', {
        duration: 2000
      })
    })
  }

  reject():void {
    this.userInfo.patchValue(this.userDetails);
  }
}

class userInfo {
  email: string;
  userName: string;
  id: string;

  // constructor(data:any) {
  //   this.email = data.email;
  //   this.userName = data.userName;
  //   this.id = data.id;
  // }
}
