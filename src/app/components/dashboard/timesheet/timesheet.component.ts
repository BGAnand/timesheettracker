import { Component, OnInit } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.css']
})
export class TimesheetComponent implements OnInit {

  timeSlots: any = [];
  days: any = [];
  dates: any = [];
  dates1: any = [];
  months: any = [];
  year: any;
  status: any = [];
  dailyStatus: any = [];
  lineManagerStatus: any = [];
  project: any = {};
  date: any = {};
  dayOfDays: any = [];
  getValues: any = {};
  endValues: any = [];
  show: boolean = false;
  bsConfig: Partial<BsDatepickerConfig>;


  projects: any = [];
  model: any;
  model2: any;

  day1: any;
  month1: any;
  year1: any;

  day2: any;
  textfields = [];
  count = 0;

  constructor() { }

  ngOnInit() {
    console.log(this.model);
    this.project.name = "Al Ghurair"
    this.days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    this.months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
    for (let i = 1; i <= 31; i++) {
      this.dates.push(i)
    }
    let date = new Date();
    this.year = date.getFullYear();
    // this.dates1 = ['1-Jun-20', '2-Jun-20', '3-Jun-20', '4-Jun-20', '5-Jun-20', '6-Jun-20', '7-Jun-20'];
    this.timeSlots = [
      0.25, 0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00, 3.25, 3.50, 3.75, 4.00, 4.25, 4.50, 4.75,
      5.00, 5.25, 5.50, 5.75, 6.00, 6.25, 6.50, 6.75, 7.00, 7.25, 7.50, 7.75, 8.00, 8.25, 8.50, 8.75, 9.00, 9.25, 9.50,
      9.75, 10.00, 10.25, 10.50, 10.75, 11.00, 11.25, 11.50, 11.75, 12.00
    ];
    this.status = ['Closed', 'in-Progress', 'onHold', 'Rejected', 'Approved'];
    this.dailyStatus = ['Completed', 'In Progress', 'In Complete', 'Waiting']
    this.lineManagerStatus = ['Approved', 'Rejected', 'In Progress', 'On Hold', 'Closed'];

    this.projects = [
      { "project": "Al Ghurair" },
      { "project": "Moniic" },
      { "project": "Telechat" }
    ]
  }
  getOption(key) {

    if (key == 'sdate') {
      this.endValues = [];
      var e = document.getElementById('sdate');
      this.getValues.sDate = Number(e['options'][e['selectedIndex']].value);
      for (let i = 0; i <= 6; i++) {
        this.endValues.push(this.getValues.sDate + i)
      }
    }
    else if (key == 'edate') {
      var e = document.getElementById('edate');
      this.getValues.eDate = Number(e['options'][e['selectedIndex']].value);
    }
    else if (key == 'mon') {
      var e = document.getElementById('month');
      this.getValues.month = e['options'][e['selectedIndex']].value;
    }
    else if (key == 'year') {
      var e = document.getElementById('year');
      this.getValues.year = e['options'][e['selectedIndex']].value;
    }





    // if(this.date.startDate !== undefined && this.date.endDate !== undefined && this.date.month !== undefined && this.date.year !== undefined) {

    // }

  }

  ShowData() {
    this.dayOfDays = [];
    for (let i = this.day1; i <= this.day2; i++) {
      if (i <= 9) {
        i = "0" + i;
      }
      let day = i + "-" + this.month1 + "-" + this.year1;
      this.dayOfDays.push(day);
    }
    this.show = true;
    console.log(this.dayOfDays)
  }

  applyLocale(date, key) {
    console.log(date)
    if (key == 'date1') {
      let myDate = new Date(date)
      this.day1 = myDate.getDate();
      this.month1 = myDate.getMonth() + 1;
      this.year1 = myDate.getFullYear();
      // if(this.day1 <= 9) {
      //   this.day1 = "0" + this.day1
      // }
      if (this.month1 <= 9) {
        this.month1 = "0" + this.month1;
      }
      console.log(this.day1, this.month1, this.year1);
    }
    else if (key == 'date2') {
      let date2 = new Date(date);
      this.day2 = date2.getDate();
      if (this.day2 <= 9) {
        this.day2
      }
      console.log(this.day2)
    }

    if (this.model !== undefined && this.model2 !== undefined) {
      this.ShowData()

    }


  }
  // addtext() {
  //   this.count++;
  //   for(let i = 0; i < 1; i++) {
  //     this.textfields.push(i);
  //   }
  // }

  selectDate(day, index) {
    if (document.getElementById('colse' + index).getAttribute('data-dismiss') == 'modal') {
      document.getElementById('colse' + index).setAttribute('data-dismiss', 'modal');
    }
    else {
      document.getElementById('colse' + + index).setAttribute('data-dismiss', 'modal');
    }

    if (this.date.day !== day) {
      this.textfields = [];
    }
    this.date.day = day;
    for (let i = 0; i < this.dayOfDays.length; i++) {
      if (day == this.dayOfDays[i]) {
        this.textfields.push(i);
      }
    }
  }


}
