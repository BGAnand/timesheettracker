import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../../service/user.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  isActive=true;
users:any=[];
filteredUsers:[];
loggedIn=false;
userInfo: FormGroup;

  constructor(private Router: Router, private matSnackBar: MatSnackBar, private userdata: UserService, snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getAllUsers();
   
  }


  @ViewChild('closebutton', { static: true }) closebutton
  isValid: boolean = true;
  createduserresponse: any;

  userForm = new FormGroup({
    userName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.minLength(6), Validators.required]),
  });

  addUser() {
    console.log(this.userForm.value);
    // document.getElementById("save_user").setAttribute('data-dismiss', 'modal');
    this.userdata.createUser(this.userForm.value).subscribe(res => {
      console.log(res);
      this.loggedIn = true;
      if (res) {
        this.closebutton.nativeElement.click()
      }
      this.matSnackBar.open('User Information Saved Sucessfully', 'close', {
        duration: 2000
      })
      //document.getElementById('CreateuserModal').style.display = 'hide'; 
      //this.createduserresponse=res;
      // if (res) {
      //   document.getElementById("save_user").setAttribute('data-backdrop', 'false');

      // }
    })
    //  if(this.createduserresponse.status===true){
    //   alert(this.createduserresponse.messageCode);
    //  }
    //  else{
    //   alert(this.createduserresponse.messageCode)
    //  }
  }

  updateUser(ind) {
    let valueAtIndex1 = this.users[ind];
    window.localStorage.removeItem("id");
    window.localStorage.setItem("id", valueAtIndex1._id.toString());

    this.userdata.updateUser(valueAtIndex1).subscribe(res => {
      console.log("Success", res)

      // this.userDetails = res.result;
      // this.userInfo.patchValue(res.result);
      this.matSnackBar.open('User info updated', 'close', {
        duration: 2000
      })
    }, err => {
      this.matSnackBar.open('Unable to update', 'please try again!', {
        duration: 2000
      })
    });
  }

deactivateUser(index):void{
    let valueAtIndex = this.users[index];
    window.localStorage.removeItem("id");
    window.localStorage.setItem("id", valueAtIndex._id.toString());
    this.userdata.deactivateUser(valueAtIndex).subscribe(res => {console.log("Success", res)
    //this.icons=false;
    console.log(res.result.active)
    console.log(res.result._id)
    this.getAllUsers();
    //.getAllUsers();
       this.matSnackBar.open('User deactivated sucessfully','close',{
        duration: 2000
       })
     },err => {
      this.matSnackBar.open('Unable to Delete','please try again!', {
         duration: 2000
       })
      });
     
  }
  activateUser(index){
    let valueAtIndex = this.users[index];
    window.localStorage.removeItem("id");
    window.localStorage.setItem("id", valueAtIndex._id.toString());
    this.userdata.activateUser(valueAtIndex).subscribe(res => {console.log("Success", res)
    console.log(res.result.active)
    console.log(res.result._id)
    this.getAllUsers();
    //this.getAllUsers(false);
    //this.icons=false;
       this.matSnackBar.open('User activated sucessfully','close',{
        duration: 2000
       })
     },err => {
      this.matSnackBar.open('Unable to Delete','please try again!', {
         duration: 2000
       })
      });
      
  }

  getAllUsers(){
    
    this.loggedIn=true
    //this.isActive=data;
    this.userdata.getAllUsersDetails().subscribe(data=>
     // let jsonObject = res.json() as Object;
      this.users=data.result
      
    );
   
  
  }

  getActiveUsers(data1){
    this.isActive=data1; 
  }
 
  // get filteruseractive(){
  //   return this.users.filter( x => x.active === true);

  // }
  // get filteruserinactive(){
  //   return this.users.filter( x => x.active !== true);
  // }

}
