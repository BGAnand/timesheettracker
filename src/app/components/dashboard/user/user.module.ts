import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import {MatFormFieldModule,MatInputModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
  }
];

@NgModule({
  declarations: [UserComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule
  ]
})
export class UserModule { }
