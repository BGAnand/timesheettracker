import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import * as Highcharts from 'highcharts';
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  highcharts = Highcharts;
  chartOptions = {   
     chart : {
        plotBorderWidth: null,
        plotShadow: false
       
     },
     credits: {
      enabled: false
    },
     title : {
        text: 'Technologies used'   
     },
     tooltip : {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
     },
     plotOptions : {
        pie: {
           allowPointSelect: true,
           cursor: 'pointer',
     
           dataLabels: {
              enabled: false           
           },
     
           showInLegend: true
        }
     },
     series : [{
        type: 'pie',
        name: 'share',
        data: [
           ['Java',   45.0],
           ['JavaScript',       26.8],
           {
              name: 'Typescript',
              y: 12.8,
              sliced: true,
              selected: true
           },
           ['Go',    8.5],
           ['PHP',     6.2],
           ['Shell',      0.7]
        ]
     }]
  };

  highcharts1 = Highcharts;
  chartOptions1 = {   
     chart: {
        type: 'bar'
     },
     title: {
        text: 'Project progress chart'
     },
     
     legend : {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 250,
        y: 100,
        floating: true,
        borderWidth: 1,
     },
      
        xAxis:{
           categories: ['Project1', 'project2', 'project3'], title: {
           text: null
        } 
     },
     yAxis : {
        min: 0, title: {
           text: 'Progress (percentage)', align: 'high'
        },
        labels: {
           overflow: 'justify'
        }
     },
     tooltip : {
        valueSuffix: '%'
     },
     plotOptions : {
        bar: {
           dataLabels: {
              enabled: true
           }
        }
     },
     credits:{
        enabled: false
     },
     series: [
        {
           name: 'Jhon',
           data: [60, 10, 35]
        }
     ]
  };


}
