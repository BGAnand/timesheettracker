import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
}                           from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { UserService } from 'src/app/service/user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  constructor(private router:Router,private userService:UserService,private snackBar:MatSnackBar) { }
 

  ngOnInit() {
  }
isValid:boolean=true;
  users =[{
    name:"pavan",
    password:"pavanpav"
  },
{
  name:"anand",
  password:"anandpa"
}
]

  profileForm = new FormGroup({
    email: new FormControl('',Validators.required),
    password: new FormControl('',[Validators.minLength(6),Validators.required]),
  });
  
onSubmit(){

  this.userService.userLogin(this.profileForm.value).subscribe(res => {
    localStorage.setItem('access_token',res.result.token);
    localStorage.setItem('id',res.result.id);
    localStorage.setItem('email',res.result.email);
    console.log(res.result.token)
    this.router.navigate(['/dashboard']);
  
  },err => {
    this.isValid = false;
  })

  
  // this.users.filter((hero)=> {
    
  //    if(hero.name === this.profileForm.value.name && hero.password === this.profileForm.value.password){
  //     this.router.navigate(['/dashboard']);
  //    }else if(hero.name !== this.profileForm.value.name){
  //     setTimeout(()=>{this.router.navigate['/'];
  //     this.isValid=false;},500);
      
  //    }
    
  // }); 
 
  
}


}
