import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {UserService} from '../service/user.service'
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-request-reset',
  templateUrl: './request-reset.component.html',
  styleUrls: ['./request-reset.component.css']
})
export class RequestResetComponent implements OnInit {

  constructor(private router:Router, private userService:UserService,private matSnackBar:MatSnackBar) {
  
   }

  isValid:boolean=true;
  forbiddenEmails: any;
  RequestResetForm:FormGroup;
  errorMessage: string;
  //successMessage: string;

  ngOnInit() {
    this.RequestResetForm= new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails)
    })
  }
  
  RequestResetUser(form) {
    console.log(form.value.email)
    if (form.valid) {
      this.isValid = true;
      this.userService.requestReset(this.RequestResetForm.value).subscribe(
        data => { 
          this.RequestResetForm.reset();
          this.matSnackBar.open('Reset password link send to your email sucessfully','close',{
            duration: 2000
           })
         // this.successMessage = "Reset password link send to email sucessfully.";
          setTimeout(() => {
            //this.successMessage = null;
            this.router.navigate(['login']);
          }, 3000);
        },
        err => {

          if (err.error.message) {
            this.matSnackBar.open( this.errorMessage = err.error.message,'close',{
              duration: 2000
             })
            //this.errorMessage = err.error.message;
          }
        }
      );
    } else {
      this.isValid = false;
    }
  }

}
