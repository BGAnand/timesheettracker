import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

    constructor(private snackBar: MatSnackBar) { };

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let reqClone;
        if (localStorage.getItem('access_token') != null) {
            reqClone = request.clone({ setHeaders: { 'x-access-token': localStorage.getItem('access_token') } });
        }
        else {
            reqClone = request.clone();
        }


        return next.handle(reqClone).pipe(catchError((error: HttpErrorResponse) => {
            console.error(error);
            if (error.status === 400) {

            } else if (error.status === 401) {

            } else if (error.status === 403) {
                this.snackBar.open(error.error.message,'Try again!',{
                    duration: 2000
                });
            }
            return throwError(error);
        })
        );

        // const token: string = localStorage.getItem('token');

        // if (token) {

        //     request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        // }

        // if (!request.headers.has('Content-Type')) {
        //     request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        // }

        // request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

        // return next.handle(request).pipe(
        //     map((event: HttpEvent<any>) => {
        //         if (event instanceof HttpResponse) {
        //             console.log('event--->>>', event);
        //         }
        //         return event;
        //     }));
    }
}