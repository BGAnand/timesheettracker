import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AuthGuardGuard } from './util/authGuard/auth-guard.guard';
import { UserComponent } from './components/dashboard/user/user.component';
import { RequestResetComponent } from './request-reset/request-reset.component';
import { ResponseResetComponent } from './response-reset/response-reset.component';



const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  // { path: 'user', component: UserComponent },
  { path: 'requestReset', component: RequestResetComponent },
  { path: 'responseReset/:resettoken', component: ResponseResetComponent },
  { path: 'dashboard', loadChildren: './components/dashboard/dashboard.module#DashboardModule',canActivate: [AuthGuardGuard] },
  {
		path: "*",
		redirectTo: "/login"
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
