import { Component, OnInit, ViewChild  } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../service/user.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  changepwd: any = {};
  message1: any;
  message2: any;

  @ViewChild('closelogout', {static:true}) closelogout

  constructor(private router: Router, private service: UserService, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  logout() {
    localStorage.clear();
    this.closelogout.nativeElement.click()
    this.router.navigate(['/']);
  }

  changePassword() {
    document.getElementById('close').setAttribute('data-dismiss', 'modal');
    this.service.changePassword(this.changepwd).subscribe(res => {
      if (res['status'] === true) {
        console.log('res',res)
        this.snackBar.open(res['messageCode'], 'Undo');
      }
    })

  }
 


}
