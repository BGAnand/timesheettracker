import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {UserService} from '../service/user.service'
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-response-reset',
  templateUrl: './response-reset.component.html',
  styleUrls: ['./response-reset.component.css']
})
export class ResponseResetComponent implements OnInit {

  ResponseResetForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  resetToken: null;
  CurrentState: any;
  IsResetFormValid = true;

  constructor(
    private Userservice: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private matSnackBar:MatSnackBar
  ) { 
     //this.CurrentState = 'Wait';
     //this.route.params.subscribe(params => {
       //this.resetToken = params.token;
       //console.log(this.resetToken);
       //this.VerifyToken();
     //});
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.resetToken = params.resettoken
    })
    this.Init();
  }

  //  VerifyToken() {
  //    this.Userservice.responseReset({ resettoken: this.resetToken }).subscribe(
  //      data => {
  //        this.CurrentState = 'Verified';
  //      },
  //     err => {
  //       this.CurrentState = 'NotVerified';
  //      }
  //    );
  //  }

   Init() {
    // this.ResponseResetForm= new FormGroup({
    //   newPassword: new FormControl(null, [Validators.required, Validators.minLength(4)]),
    //   confirmPassword: new FormControl(null, [Validators.required, Validators.minLength(4)]),
    // })

      this.ResponseResetForm = this.fb.group(
        {
          // resettoken: [this.resetToken],
          password: ['', [Validators.required, Validators.minLength(4)]],
          confirmPassword: ['', [Validators.required, Validators.minLength(4)]]
        }
      );
   }

   Validate(passwordFormGroup: FormGroup) {
     const new_password = passwordFormGroup.controls.password.value;
     const confirm_password = passwordFormGroup.controls.confirmPassword.value;

     if (confirm_password.length <= 0) {
       return null;
     }

     if (confirm_password !== new_password) {
       return {
         doesNotMatch: true
       };
     }

     return null;
   }

   ResetPassword(form) {
     console.log(form.get('confirmPassword'));
     if (form.valid) {
       this.IsResetFormValid = true;
       this.Userservice.responseReset(this.ResponseResetForm.value, this.resetToken).subscribe(
         data => {
           this.ResponseResetForm.reset();
           this.matSnackBar.open('New Password Saved Sucessfully','close',{
            duration: 2000
           })
           //this.successMessage = data.message;
           setTimeout(() => {
             this.router.navigate(['login']);
           }, 3000);
         },
         err => {
         if (err.error.message) {
          this.matSnackBar.open( this.errorMessage = err.error.message,'close',{
            duration: 2000
           })
    
           }
         }
       );
     } else { this.IsResetFormValid = false; }
   }

}
